# Description



## Installation

Use the package manager [node](https://nodejs.org/en/) to install ViEdu.

npm install —save


## Running

npm start

## And coding

Coding based on Coding covention below:
https://docs.google.com/document/d/1NJlM0YghcKgKi1dNGU_rzLl9PWBeEPpW230hc7deX-8/edit

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[MIT](https://choosealicense.com/licenses/mit/)
