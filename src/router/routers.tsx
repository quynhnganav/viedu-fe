import { lazy } from "react";
import { PRIVATE_ROUTE, PUBLIC_ROUTE } from "constant";
const routerNoAuth = [
    {
        path: PUBLIC_ROUTE.LOGIN,
        exact: false,
        isAuth: false,
        component: lazy(() => import('../components/PublicPages/Login/Login'))
    },
    {
        path: PUBLIC_ROUTE.FORGOT_PASSWORD,
        exact: false,
        isAuth: false,
        component: lazy(() => import('../components/PublicPages/ForgetPass/ForgetPass'))
    },
    {
        path: PUBLIC_ROUTE.RESET_PASSWORD,
        exact: false,
        isAuth: false,
        component: lazy(() => import('../components/PublicPages/ResetPass/ResetPass'))
    },
    
]

const routerAuth: any[] = [
    {
        path: '/admin',
        exact: false,
        isAuth: true,
        component: lazy(() => import('../components/PrivatePages'))
    },
    {
        path: '/home',
        exact: false,
        isAuth: true,
        component: lazy(() => import('../components/PrivatePages/GeneralNoti/GeneralNoti'))
    },
    {
        path: PRIVATE_ROUTE.SEARCH_STUDENT,
        exact: false,
        isAuth: true,
        component: lazy(() => import('../components/PrivatePages/SearchStudents/SearchStudent'))
    },
    {
        path: '/calendar',
        exact: false,
        isAuth: true,
        component: lazy(() => import('../components/PrivatePages/Calendar/Calendar'))
    }
    
]

const routerAdmin = [
    
    
]

export {
    routerNoAuth,
    routerAuth,
    routerAdmin
}