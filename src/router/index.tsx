import React, { Suspense } from "react";
import {
    BrowserRouter as Router,

} from "react-router-dom";
import RouterContainer from "./renderRoute";


const RouterRoot = () => {


    return (
        <Router>
            <Suspense fallback={"Loading..."}>
                <RouterContainer />
            </Suspense>
        </Router >
    )
}
export {
    RouterRoot
}