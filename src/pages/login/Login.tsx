import React, { memo } from "react";
import { Row, Col, Button, Checkbox } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import LoginForm from "../../components/login/loginForm/LoginForm";
import './login.scss';
import RightSideLogin from "../../components/login/rightSide/RightSideLogin";
import Logo from '../../assets/img/stem-logo.jpg';
const Login = () => {
    const onFinish = (values: any) => {
        console.log('Received values of form: ', values);
    };
    return (
        <Row>
            <Col span={12} className='left'>
                <div className='logo-img-container'>  <img  className='logo-login' src={Logo}/></div>
          
                <LoginForm></LoginForm></Col>
              
            <Col span={12}>
                <RightSideLogin />
            </Col>

        </Row>


    )
}
export default memo(Login);