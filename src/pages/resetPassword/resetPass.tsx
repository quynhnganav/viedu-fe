import React, { memo } from "react";
import RightSideLogin from "../../components/login/rightSide/RightSideLogin";
import { Row, Col, Button, Checkbox } from 'antd';
import ResetPassForm from "../../components/resetPass/form/resetPassForm";
import './resetPass.scss'
const ForgotPass = () => {
    return (
       <Row>
           <Col span={12} className='left'><ResetPassForm /></Col>
           <Col span={12}><RightSideLogin /></Col>
       </Row>
    )
}
export default memo(ForgotPass);