import en from "./translations/en.json";
import vi from "./translations/vi.json";

const languageObject = { en, vi };
export default languageObject;