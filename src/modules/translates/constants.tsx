
export const SYSTEM_TRANSLATE = "SYSTEM_TRANSLATE";
export const CHANGE_LOCALE_LANGUAGE_SUCCESS = "CHANGE_LOCALE_LANGUAGE_SUCCESS";
export const SWITCH_LANGUAGE_SUCCESS = "SWITCH_LANGUAGE_SUCCESS";

export const LANG_PARAM = "lang";
export const LOCALE_ARRAY = ["en", "zh"];

export const LANGUAGE_CODE_DEFAULT = "en";

export const LIST_LANG = [
  {
    lang: "en",
    name: "homeComponent.en",
    icon: "icon-en"
  },
  
 {
     lang: "vi",
     name: "homeCompnent.vi",
     icon: "icon-vi"
 }
];