import React, { memo, useState, Suspense } from "react";
import { Layout, Avatar, Menu  } from 'antd';
import { Link, Switch,  Route, useRouteMatch } from "react-router-dom";
import { routerAuth } from "../../router/routers";
import {
    MenuUnfoldOutlined,
    MenuFoldOutlined,
    UserOutlined,
    VideoCameraOutlined,
    BellOutlined,
    MenuOutlined,
} from '@ant-design/icons';
import "./home.scss";
import MenuHome from "components/Molecules/Menu/MenuHome";
import { PRIVATE_ROUTE } from "constant";
import { Badge } from "antd";
const { Header, Sider, Content } = Layout;


const Home = () => {


const { SubMenu } = Menu;
    
    let { path, url } = useRouteMatch();
    console.log('hello', path, url);
    console.log(routerAuth)
    const [collapsed, setCollapsed] = useState(false);
    const toggle = () => {
        setCollapsed(!collapsed)
    }
    return (<>
        <Layout style={{
            minHeight: '100vh'
            
        }}>
            <Sider
            breakpoint='lg'
            onBreakpoint={() => {
                toggle()
            }}
                className='menu'
                trigger={null} collapsible collapsed={collapsed}>
                <div className="logo"><Link
                    className='logo-link'
                    to={PRIVATE_ROUTE.HOME}
                >{collapsed ? <></> : 'stem square'}</Link></div>
                <MenuHome />
                
            </Sider>
            <Layout className="site-layout">
                <Header className="site-layout-background header-container" style={{ padding: 0 }}>
                    {React.createElement(collapsed ? MenuOutlined : MenuOutlined, {
                        className: 'trigger',
                        onClick: toggle,
                    })}
                    <div>
                        <Badge count={0} showZero >
                            <BellOutlined style={{ fontSize: 22 }} />
                        </Badge>

                        <Avatar size={40}>N</Avatar>

                    </div>
                </Header>
                <Content
                    className="site-layout-background"
                    style={{
                         margin: '24px 15px',
                    }}
                >
                    <Suspense fallback="loading">
                            <Switch>
                                {
                                    routerAuth.map(r =>
                                        <Route key={`${path}${r.path}`} exact={r.exact} path={`${path}${r.path}`} >
                                            <r.component />
                                            
                                        </Route>
                                    )
                                }
                            </Switch>
                        </Suspense>
          </Content>
            </Layout>
        </Layout>
    </>)

}


export default Home;