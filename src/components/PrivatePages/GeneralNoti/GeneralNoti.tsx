import React, { memo, useEffect } from "react";
import InformationGeneral from "components/Organisms/InformationGeneral/InformationGeneral";
import NotificationInfoPanel from "components/Organisms/NotificationInfoPanel/NotificationInfoPanel";
import { Table, Tag, Row, Col } from "antd";
import { Pie } from '@ant-design/charts';
import './general-noti.scss';
const GeneralNoti = () => {
  let ref;

  const renderTitle = (title: string) => {
    return (
    <p className='title-style'>{title}</p>
    )

  }

  const classData = [
    {
      type: 'CENTER',
      value: 10,
    },
    {
      type: 'COPP',
      value: 2,
    },
    {
      type: 'ONLINE',
      value: 1,
    },
    {
      type: 'SCHOOL',
      value: 19,
    },
   
  ];
 
  
  const classConfig = {
    appendPadding: 10,
    data: classData,
    angleField: 'value',
    colorField: 'type',
    radius: 0.8,
    label: {
      type: 'inner',
      offset: '-30%',
      content: '{name}',
      style: {
        fontSize: 14,
        textAlign: 'center',
      },
    },
    state: {
      active: {
        style: {
          lineWidth: 0,
          fillOpacity: 0.65,
        },
      },
    },
    interactions: [{ type: 'element-selected' }, { type: 'element-active' }],
  };
  useEffect(() => {
    if (ref) {
      ref.setState('active', (classData) => classData.type === 'SCHOOL');
      ref.setState('selected', (classData) => classData.type === 'ONLINE' || classData.type === 'COPP');
    }
  }, []);
  const dataChart = [
    {
      type: 'INITIAL',
      value: 27,
    },
    {
      type: 'CONSULTING',
      value: 25,
    },
    {
      type: 'LEARNING',
      value: 18,
    },
    {
      type: 'TAKE_A_BREAK',
      value: 15,
    },
    {
      type: 'STOP',
      value: 10,
    },
    {
      type: 'NO_FOLLOW',
      value: 5,
    },
    {
      type: 'Waiting',
      value: 5,
    },
  ];
  const pieChartConfig = {
    appendPadding: 10,
    data: dataChart,
    angleField: 'value',
    colorField: 'type',
    radius: 0.8,
    label: {
      type: 'spider',
      labelHeight: 28,
      content: '{name}\n{percentage}',
    },
    interactions: [{ type: 'element-selected' }, { type: 'element-active' }],
  };
  const data = [
    {
      key: '1',
      name: 'John Brown',
      age: 32,
      address: 'New York No. 1 Lake Park',
      tags: ['nice', 'developer'],
    },
    {
      key: '2',
      name: 'Jim Green',
      id: '00045',
      phone: '0703659254',
      email: 'quynhnganav@gmail.com',
      tags: ['loser'],
    },
    {
      key: '3',
      name: 'Joe Black',
      age: 32,
      address: 'Sidney No. 1 Lake Park',
      status: 'initial'
    },
  ];

  const columns = [
    {
      title: 'Id',
      dataIndex: 'id',
      key: 'id',

    },
    {
      title: 'Phone',
      dataIndex: 'phone',
      key: 'phone',
    },
    {
      title: 'Email',
      dataIndex: 'email',
      key: 'email',
    },
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',

    },
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
      render: text => <Tag color='geekblue'>
        {text}
      </Tag>
    },
    {
      title: 'Action',
      key: 'action',
      render: (value: any) => (

        <a>...</a>


      ),
    },
  ];
  const classCol = [
    {
      title: 'Date',
      dataIndex: 'date',
      key: 'day',

    },
    {
      title: 'Class',
      dataIndex: 'class',
      key: 'class',
    },
    {
      title: 'TotalStudent',
      dataIndex: 'totalStudent',
      key: 'totalStudent',
    },
    {
      title: 'Attendant',
      dataIndex: 'attendant',
      key: 'attendant',

    }

  ];


  return (
    <div>
      <InformationGeneral></InformationGeneral>
      <NotificationInfoPanel></NotificationInfoPanel>
      <Row className='mt-3'>
        <Col span={12}  ><Table className='bg-white mx-1 p-2 border-1px' title={() => renderTitle('Thông tin tài khoản đăng kí hoặc đã thay đổi')} columns={columns} dataSource={data} /></Col>
        <Col span={12} > <Table className='mx-1 bg-white p-2 border-1px'   title={() => renderTitle('Thông tin lớp đi học')} columns={classCol} dataSource={data} /></Col>

      </Row>
      <Row className='mt-3'
      >
        <Col span={12} ><Pie className='bg-white mx-1 p-1 border-1px' {...pieChartConfig} /></Col>
        <Col span={12} > <Pie className='bg-white mx-1 p-1 border-1px'  {...classConfig} /></Col>

      </Row>
    </div>
  )

}
export default memo(GeneralNoti);