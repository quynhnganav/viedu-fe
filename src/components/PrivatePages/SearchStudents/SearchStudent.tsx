import SearchBox from 'components/Atoms/SearchBox/SearchBox';
import SearchStudentForm from 'components/Organisms/SearchStudent/SearchStudentForm';
import React, { memo } from 'react';
import { Table } from 'antd';
import  "./search-student.scss";



const SearchStudent = () => {
  const dataSource = [
    {
      key: '1',
      studentId: 9000,
      name: 'Nga',
      school: 'ABC',
      class: '12s3',
      classAtSchool: '124',
      status: 'INITIAL'
    },
    {
      key: '1',
      studentId: 9000,
      name: 'Nga',
      school: 'ABC',
      class: '12s3',
      classAtSchool: '124',
      status: 'INITIAL'
    },
    {
      key: '1',
      studentId: 9000,
      name: 'Nga',
      school: 'ABC',
      class: '12s3',
      classAtSchool: '124',
      status: 'INITIAL'
    },
    {
      key: '1',
      studentId: 9000,
      name: 'Nga',
      school: 'ABC',
      class: '12s3',
      classAtSchool: '124',
      status: 'INITIAL'
    },
    {
      key: '1',
      studentId: 9000,
      name: 'Nga',
      school: 'ABC',
      class: '12s3',
      classAtSchool: '124',
      status: 'INITIAL'
    },
    {
      key: '1',
      studentId: 9000,
      name: 'Nga',
      school: 'ABC',
      class: '12s3',
      classAtSchool: '124',
      status: 'INITIAL'
    },
    {
      key: '1',
      studentId: 9000,
      name: 'Nga',
      school: 'ABC',
      class: '12s3',
      classAtSchool: '124',
      status: 'INITIAL'
    },

  ];
  
  const columns = [
    {
      title: 'Mã HS',
      dataIndex: 'studentId',
      key: 'studentId',
    },
    {
      title: 'Tên',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Mã Trường',
      dataIndex: 'school',
      key: 'school',
    },
    {
      title: 'Lớp',
      dataIndex: 'class',
      key: 'class',
    },
    {
      title: 'Lớp ở trường',
      dataIndex: 'classAtSchool',
      key: 'classAtSchool',
    },
    {
      title: 'Tình Trạng',
      dataIndex: 'status',
      key: 'status',
    },
  ];
  
    return (
    <>
  <SearchStudentForm />
  <Table  className = 'mt-4 table-striped-rows' dataSource={dataSource} columns={columns}/>
    </>)
}
export default memo(SearchStudent)