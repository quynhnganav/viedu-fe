import React, { memo } from 'react';
import { Card, Statistic } from "antd";
import {  ArrowUpOutlined } from "@ant-design/icons";

const Reporting = props => {
    return (<Card>
        <Statistic
          title="Active"
          value={11.28}
          precision={2}
          valueStyle={{ color: '#3f8600' }}
          prefix={<ArrowUpOutlined />}
          suffix="%"
        />
      </Card>)

};
export default memo(Reporting)