import React, { memo, useState, useEffect } from "react";
import { Form, Checkbox } from 'antd';
import { Link } from "react-router-dom";
import classnames from 'classnames';
import _ from "lodash";
import FormFields from "components/Atoms/FormFields/FormFields";
import RemeberLoggedContainer from "components/Atoms/RemeberLoggedContainer/RememberLoggedContainer";
import ButtonWithIcon from "components/Atoms/Button/ButtonWithIcon";


const FormInput = props => {
  const renderFormFields = () => {
    return _.map(props.formDetail, ({ formItemName, label, isRequired, ruleMessage, placeholder, inputIcon }, index) => {
      return (
        <FormFields
        key={formItemName}
          formItemName={formItemName}
          label={label}
          isRequired={isRequired}
          ruleMessage={ruleMessage}
          placeholder={placeholder}
          inputIcon={inputIcon}
        />
      )
    })
  }
  return (
    <Form
      name={props.formName}
      onFinish = {props.formFinish}
      onFinishFailed={props.formFinishFailed}
      initialValues={{ remember: true }}
    >
      { renderFormFields()}
      {props.formName == 'login' ? <RemeberLoggedContainer /> : <></>}
      <Form.Item>
        <ButtonWithIcon
          htmlType='submit'
          buttonStyle={props.buttonStyle}
          rightIcon={props.buttonIcon}
          buttonContent={props.buttonContent} />
      </Form.Item>
    </Form>
  )
}
export default memo(FormInput)