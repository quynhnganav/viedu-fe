import react, { memo } from "react";
import { Card } from "antd";
import './information-genetal-card.scss';
import Icon, { UserOutlined, DownOutlined } from "@ant-design/icons";
import classnames from "classnames";



const InformationGeneralCard = props => {

    return (

        <Card className=' border-1px container card-container col-sm-12 col-md-6 col-lg-6 col-xl-3'>
            <div className='icon-wrap'>
                <div className={classnames('icon-wrap-background rounded-circle', props.bg)} />
                {props.icon}
            </div>
            <div className='content-wrap mw-100'>
                <p className='sub-heading text-uppercase'>{props.label}</p>
                <span className='number'>1.7M</span>
            </div>

        </Card>


    )

}
export default memo(InformationGeneralCard);