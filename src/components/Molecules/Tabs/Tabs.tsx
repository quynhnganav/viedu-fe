import { Tabs } from 'antd';
import React, { memo } from "react";
import _ from "lodash";

const Tab = props => {
  const { TabPane } = Tabs;
  const renderTabPane = () => {
    return _.map(props.tabDetail, ({ tabName, tabContent }, index) => {
      return (
        <TabPane tab={tabName} key={index}>
         {tabContent}
        </TabPane>
      )
    })
  }
  return (
    <Tabs defaultActiveKey="1" >
      {renderTabPane()}
    </Tabs>
  );
}

export default memo(Tab);