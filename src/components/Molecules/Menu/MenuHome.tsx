import React, { memo } from 'react';
import { Menu } from 'antd';
import { Link, useRouteMatch } from "react-router-dom";
import _ from "lodash";
import './menu-home.scss';
import menuItems from "./menuItems";
import { PRIVATE_ROUTE } from 'constant';

const MenuHome = props => {
  let { path, url } = useRouteMatch();

  const renderMenuItems = () => {
    console.log(url);
    return _.map(menuItems, ({ key, icon, menuItem }) => {
      return (
        <Menu.Item
          key={key}
          icon={icon}
        >
        <Link to={`${url}/${key}`}>{menuItem}</Link>
        </Menu.Item>
      )
    })
  }
  return (
    <Menu
      className='menus'
      theme="dark" mode="inline" defaultSelectedKeys={['1']}>
      {renderMenuItems()}
    </Menu>
  )
}
export default memo(MenuHome);