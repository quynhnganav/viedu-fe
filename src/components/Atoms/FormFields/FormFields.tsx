import React,{ memo } from "react";
import _ from "lodash";
import { Form, Input } from 'antd';
import "./form-fields.scss";

const FormFields = props => {
    
      return (
        <Form.Item
          name={props.formItemName}
          label={props.label}
          labelAlign="left"
          rules={[{ required: props.isRequired, message: props.ruleMessage }]}
        >
          <Input prefix={props.inputIcon} placeholder={props.placeholder} />
        </Form.Item>

      )

  }
  export default FormFields;