import React, { memo } from 'react';
import { Input } from 'antd';
import { SearchOutlined } from "@ant-design/icons";
import './search-box.scss';

const SearchBox = props => {
  const Search = Input.Search;
  return (
    <div>
      <label className='font-14 pl-1 mb-1'>{props.label}</label>
      <Input
   
     className='w-100'
      allowClear
        prefix={<SearchOutlined />}
        placeholder="Placeholder"
      />

    </div>)
}
export default memo(SearchBox);