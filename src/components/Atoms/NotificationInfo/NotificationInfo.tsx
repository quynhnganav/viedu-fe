import React, { memo } from 'react';
const NotificationInfo = () => {
    return (
        <div>

            <div>
                <p>3/7/2021 12:00:00 AM - 05/03/2021</p>
                <p>TRUNG TÂM ĐÃ NỘP BÀI THI FLL ĐẾN BAN TỔ CHỨC TẠI HỒ CHÍ MINH</p>
            </div>
            <p>Sau khi tập luyện, các bạn tại trung tâm và các trường học đã nộp bài thi FLL đến Ban tổ chức cuộc thi FLL 2021 do Sở GD TPHCM và Đà Nẵng tổ chức, chúng ta còn chờ đợi để có kết quả. Trong trường hợp vào chung kết, thì các bạn sẽ tiếp tục vào Hồ Chí Minh để thi đấu tranh tài cùng với các bạn khác, để có vé chung kết đ Mỹ năm 2021-2022 (nếu dịch Covid không xảy ra)</p>
        </div>)

};
export default memo(NotificationInfo);