import React, { memo } from "react";
import { Button } from 'antd';
import "./button-with-icon.scss";
import classnames from 'classnames';

const LoginButton = props => {
    return (
        <Button
            className={classnames('login-button', props.buttonStyle)}
            onClick={props.clickButton}
            htmlType={props.htmlType}
        >
            <div>  {props.leftIcon}</div>
            <div> {props.buttonContent} </div>
            <div> {props.rightIcon} </div>
        </Button>
    )

}
export default memo(LoginButton);