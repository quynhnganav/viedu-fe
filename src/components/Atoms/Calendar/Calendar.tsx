import React, { useState } from "react";
import { Calendar, momentLocalizer } from "react-big-calendar";
import moment from "moment";
import withDragAndDrop from "react-big-calendar/lib/addons/dragAndDrop";
import "react-big-calendar/lib/addons/dragAndDrop/styles.css";
import "react-big-calendar/lib/css/react-big-calendar.css";

const localizer = momentLocalizer(moment);
const DnDCalendar = withDragAndDrop(Calendar);

const MyCalendar = props =>{
    const [start, setStart] = useState(moment().toDate());
    const [end, setEnd] = useState(moment().add(1, "days").toDate());
    const [title, setTitle] = useState('someTitle');

const onEventResize = (data: any) => {
    const { start, end } = data;
    setStart(start);
    setEnd(end);
}
const events = [
    {
      start: start,
      end: end,
      title: 'title',
    }
  ]

//   onEventResize = (data) => {
//     const { start, end } = data;

//     this.setState((state) => {
//       state.events[0].start = start;
//       state.events[0].end = end;
//       return { events: [...state.events] };
//     });
//   };
const onEventDrop = (data) => {
    console.log(data);
}
  

 
    return (
      <div className="App">
        <DnDCalendar
          defaultDate={moment().toDate()}
          defaultView="month"
          events={events}
          localizer={localizer}
          onEventDrop={onEventDrop}
        //   onEventResize={this.onEventResize}
          resizable
          style={{ height: "100vh" }}
        />
      </div>
    );
  
}

export default MyCalendar;