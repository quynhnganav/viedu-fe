import React, { memo } from "react";
import { Select } from 'antd';
import _ from "lodash";
import './select-box.scss';


const SelectBox = props => {
    const { Option } = Select;

    const renderOption = () => {
        return _.map(props.option, ({ value }, index) => {
            return (

                <Option value={value}
                    key={index}
                >Jack</Option>

            )
        })
    }



    return (
        <div >
            <label className='font-14 pl-1 mb-1'>{props.label}</label>
            <Select
                className='w-100 no-border-radius'
                showSearch
                placeholder="Select a person"
                optionFilterProp="children"
                // onChange={onChange}
                // onFocus={onFocus}
                // onBlur={onBlur}
                // onSearch={onSearch}
                filterOption={(input, option) =>
                    option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                }
            >
                {renderOption()}
            </Select>
        </div>)

}
export default memo(SelectBox)