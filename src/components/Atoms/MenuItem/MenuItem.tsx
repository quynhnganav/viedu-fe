import React, { memo } from "react";
import { Menu } from "antd";
const MenuItem = props => {
    return (
        <Menu.Item key={props.key} icon={props.icon}>
             {props.menuItem}
            </Menu.Item>
    )

}
export default memo(MenuItem);