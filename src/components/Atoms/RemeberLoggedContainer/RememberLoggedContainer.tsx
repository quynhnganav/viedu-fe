import React, { memo } from "react";
import { Form, Checkbox } from "antd";
import { Link } from "react-router-dom";
import './remember-logged.scss';
import { PUBLIC_ROUTE } from "../../../constant/routes";

const RememberLoggedConatiner = () => {
    return (
        <Form.Item className='remember-logged-container'>
        <Form.Item name="remember" valuePropName="checked" noStyle>
          <Checkbox>Keep me logged in</Checkbox>
        </Form.Item>
        <Link to={PUBLIC_ROUTE.FORGOT_PASSWORD} className='forgot-pass-link'>
          Forgot your password
      </Link>
      </Form.Item >
    )
}
export default memo(RememberLoggedConatiner)