import React, { memo } from "react";
import PublicTemplete from "components/Templetes/PublicTemplete/PublicTemplete";
import FormFields from './formFields';
import { ArrowRightOutlined } from "@ant-design/icons";
import { useHistory } from "react-router-dom";
import './login.scss';
import { PRIVATE_ROUTE } from "constant/routes";

const Login = () => {
  const history = useHistory();

  const formFinish = () => {
    history.push('/admin/home')
  }
  return (
    <PublicTemplete
      formTitle='Welcome Back'
      dividerText='OR LOGIN WITH YOUR ACCOUNT'
      dividerTextStyle='divider-style'
      formName='login'
      formDetail={FormFields}
      buttonStyle='button-container'
      buttonContent='LOGIN'
      buttonIcon={<ArrowRightOutlined />}
      noAccount='Sign up here'
      noAccountText='Dont have an account?'
      formFinish={() => formFinish()}
      htmlType='submit'
    />

  )
}
export default memo(Login);