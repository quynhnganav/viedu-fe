
import { UserOutlined, LockOutlined } from "@ant-design/icons";
export default [
    { formItemName: 'username', label: 'Username', isRequired: true, ruleMessage: 'You need to enter your username', placeholder:'UserName', inputIcon: <UserOutlined />},
    { formItemName: 'password', label: 'Password', isRequired: true, ruleMessage: 'You need to enter your password', placeholder:'Password', inputIcon: <LockOutlined /> },
   
  ];
