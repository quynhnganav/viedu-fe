import React, { memo } from 'react';
import PublicTemplete from "components/Templetes/PublicTemplete/PublicTemplete";
import FormFields from "./formFields";
import { ArrowRightOutlined } from "@ant-design/icons";
import "./reset-pass.scss";
import { useHistory } from 'react-router';
import { PRIVATE_ROUTE, PUBLIC_ROUTE } from 'constant';
const ResetPass = () => {
    const history = useHistory();

    const formFinish = () => {
        history.push(PRIVATE_ROUTE.HOME)

    }
    return ( <PublicTemplete 
        formTitle = 'forgot your password? '
        dividerText='Enter code number sent to your Email address'
        dividerTextStyle='divider-styles'
        formName = 'resetPass'
        formDetail = {FormFields}
        buttonStyle = 'button-container'
        buttonContent='Save New Password'
        buttonIcon ={<ArrowRightOutlined  />}
        noAccount='Go back to login'
        formFinish = {() => formFinish()}
        htmlType='submit'
        linkNoAccount = {PUBLIC_ROUTE.LOGIN}
        />)

}
export default memo(ResetPass);