
import { UserOutlined, LockOutlined } from "@ant-design/icons";
export default [
    { formItemName: 'password', label: 'New password', isRequired: true, ruleMessage: 'You need to enter your password', placeholder:'Enter new password', inputIcon: <LockOutlined />},
    { formItemName: 'code', label: 'Verification code', isRequired: true, ruleMessage: 'You need to enter your code number', placeholder:'Enter verification code', inputIcon: <LockOutlined /> },
   
  ];
