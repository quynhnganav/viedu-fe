// import React, { memo } from "react";
// import PublicTemplete from "components/Templetes/PublicTemplete/PublicTemplete";
// import { ArrowRightOutlined } from "@ant-design/icons";
// import formFields from "./formFields";
// import './forget-pass.scss';

// const ForgetPass = () => {
// return (
//     <PublicTemplete 
//     formTitle = 'Forgot Your Password?'
//       dividerText='Enter your email address below and well send you a verification code to reset your password.'
//       dividerTextStyle='divider-styles'
//       formDetail = {formFields}
//       buttonStyle = 'button-container'
//       buttonContent='Send Verication Email'
//       buttonIcon ={<ArrowRightOutlined  />}

//     />
// )

// }
// export default memo(ForgetPass);
import React, { memo } from "react";
import PublicTemplete from "components/Templetes/PublicTemplete/PublicTemplete";
import FormFields from './formFields';
import { ArrowRightOutlined } from "@ant-design/icons";
import './forget-pass.scss';
import { useHistory } from "react-router-dom";
import { PUBLIC_ROUTE } from "constant/routes";


const ForgetPass = () => {
    const history = useHistory();
    const formFinsh = () => {
        history.push(PUBLIC_ROUTE.RESET_PASSWORD)
    }
    return (
        <PublicTemplete
            formFinish = {() => {formFinsh()}}
            formTitle='Forgot Your Password?'
            dividerText='Enter your email address below and well send you a verification code to reset your password.'
            dividerTextStyle='divider-styles'
            formName='forget-password'
            formDetail={FormFields}
            buttonStyle='button-container'
            buttonContent='Send Verication Email'
            buttonIcon={<ArrowRightOutlined />}
            noAccount='Go back to login'
            linkNoAccount = {PUBLIC_ROUTE.LOGIN}
            
        />

    )
}
export default memo(ForgetPass);