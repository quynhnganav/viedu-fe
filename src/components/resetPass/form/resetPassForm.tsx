import React, { memo } from "react";
import { Form, Input } from 'antd';
import { ArrowRightOutlined } from '@ant-design/icons';
import "../../login/loginForm/loginForm.scss";
import LoginButton from "../../login/button/loginButton";
import './resetPassForm.scss';

const ResetPassForm = () => {
    const onFinish = (values: any) => {
        console.log('Received values of form: ', values);
    };
    return (
        <div className='reset-form-wrap'>
            <h1>Forgot Your Password?</h1>
            <span>Enter your email address below and we'll send you a verification code to reset your password.</span>
            <Form
                name="enter-mail"
                className="enter-mail-form"
                initialValues={{ remember: true }}
                onFinish={onFinish}
            >
                <Form.Item
                    name="email"
                    label="Email Address"
                    labelAlign="left"
                    rules={[{ required: true, message: 'Please input your Email!' }]}
                >
                    <Input placeholder="Enter Email" />
                </Form.Item>





                <Form.Item>
                    <LoginButton
                        buttonStyle='login-form-button'
                        rightIcon={<ArrowRightOutlined />}
                        buttonContent='Reset Password'

                    />

                </Form.Item>
            </Form>

        </div>)
}
export default memo(ResetPassForm);