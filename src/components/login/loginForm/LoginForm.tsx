import React, { memo } from "react";
import { Form, Input, Button, Checkbox } from 'antd';
import { UserOutlined, LockOutlined, ArrowRightOutlined, GooglePlusOutlined } from '@ant-design/icons';
import "./loginForm.scss";
import { Link } from "react-router-dom";
import LoginButton from "../button/loginButton";


const LoginForm = () => {
  const onFinish = (values: any) => {
    console.log('Received values of form: ', values);
  };
  return (
    <div className='login-form-wrap'>
      
      <h1>Welcome Back</h1>
       <LoginButton 
       buttonStyle = 'login-gg-button'
        leftIcon = {<GooglePlusOutlined />}
        buttonContent='Login With Google'
        />
      <span>OR LOGIN WITH EMAIL</span>
      <Form
        name="normal_login"
        className="login-form"
        initialValues={{ remember: true }}
        onFinish={onFinish}
      >
        <Form.Item
          name="username"
          label="Username"
          labelAlign="left"
          rules={[{ required: true, message: 'Please input your Username!' }]}
        >
          <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Username" />
        </Form.Item>
        <Form.Item
          name="password"
          label="Password"
          labelAlign="left"
          rules={[{ required: true, message: 'Please input your Password!' }]}
        >
          <Input
            prefix={<LockOutlined className="site-form-item-icon" />}
            type="password"
            placeholder="Password"
          />
        </Form.Item>
        <Form.Item className='remember-logged-container'>
          <Form.Item name="remember" valuePropName="checked" noStyle>
            <Checkbox>Keep me logged in</Checkbox>
          </Form.Item>
          <Link to='/forgot-password' className='forgot-pass-link'>
            Forgot your password
        </Link>
          {/* <a className="login-form-forgot" href="">
          Forgot password
        </a> */}
        </Form.Item>

        <Form.Item>
        <LoginButton 
        buttonStyle = 'login-form-button'
        rightIcon = {<ArrowRightOutlined />}
        buttonContent='Login'

        />

        </Form.Item>
      </Form>

    </div>)
}
export default memo(LoginForm);