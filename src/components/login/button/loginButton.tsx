import React, { memo } from "react";
import { Button } from 'antd';
import { GooglePlusOutlined } from '@ant-design/icons';
import "./loginButton.scss";
import classnames from 'classnames';

const LoginButton = props => {

    return (
        <Button className={classnames('login-button', props.buttonStyle)} >
            <div>  {props.leftIcon}</div>
            <div> {props.buttonContent} </div>
            <div> {props.rightIcon} </div>
        </Button>
    )

}
export default memo(LoginButton);