import React, { memo } from 'react';
import { Col, Row, Table } from 'antd';
import SearchBox from "components/Atoms/SearchBox/SearchBox";
import SelectBox from 'components/Atoms/Select/SelectBox';

const SearchStudentForm = props => {
   
    return (
        <div>
    <Row>
        <Col span={6}><SearchBox label='Name'/></Col>
        <Col span={6}><SearchBox label='User CD' /></Col>
        <Col span={6}><SelectBox label ='Status'/></Col>
        <Col span={6}><SelectBox label ='Class'/></Col>
    </Row>
  
    </div>
    
    )

}
export default memo(SearchStudentForm);