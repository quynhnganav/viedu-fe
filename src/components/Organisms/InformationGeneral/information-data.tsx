
import { UserOutlined, StopOutlined, ReadOutlined, UserSwitchOutlined } from "@ant-design/icons";
export default [
    { icon: <UserOutlined />, bg: 'bg-warning', label: 'total users'},
    { icon: <StopOutlined />, bg: 'bg-success', label: 'total students'},
    { icon: <ReadOutlined />, bg: 'bg-info', label: 'student learning'},
    { icon: <UserSwitchOutlined />, bg: 'bg-danger', label: ' stop'}
]
 
