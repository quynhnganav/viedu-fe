import react, { memo } from "react";
import InformationGeneralCard from "components/Molecules/InformationGeneralCard/InformationGeneralCard";
import InformationData from "./information-data";
import _ from "lodash";
const InformationGeneral = () => {
  const renderInformation = () => {
    return _.map(InformationData, ({ icon, bg, label }, index) => {
      return (

        <InformationGeneralCard
          key={index}
          icon={icon}
          bg={bg}
          label={label}
        />


      )
    })
  }
  return (

    <div className='row m-0 mx-1'>
      {renderInformation()}</div>

  )

}
export default memo(InformationGeneral)