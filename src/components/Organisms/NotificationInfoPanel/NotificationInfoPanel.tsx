import React, { memo } from 'react';
import Tabs from "components/Molecules/Tabs/Tabs";
import tabFields from "./tabFields";
import './notification-info.scss';

const NotificationInfoPanel = () => {
    return(
        <div className='noti-panel-wrap bg-white mt-3 mx-1 p-3 border-1px'>
            <p className='title-style'>Thông báo</p>
             <Tabs 
             tabDetail = {tabFields}
             />
        </div>
        )
}
export default NotificationInfoPanel;