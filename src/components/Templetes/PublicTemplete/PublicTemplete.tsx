import React, { memo } from "react";
import ButtonWithIcon from "components/Atoms/Button/ButtonWithIcon";
import google from "assets/icons/google.svg";
import FormInput from "components/Molecules/Form/FormInput";
import logo from "../../../assets/img/stem-logo.jpg";
import './public-templete.scss';
import classnames from "classnames";
import { Link } from "react-router-dom";


const PublicTemplete = props => {
  return (
    <div className='wrap'>
      <div className='form-wrap'>
        <img className='logo-img' src={logo} />
        <div className='form-container'>
          <h1>{props.formTitle}</h1>
          {props.formName == 'login' ? <ButtonWithIcon
            buttonStyle='login-gg-button'
            leftIcon={<img className='google-icon' src={google} />}
            buttonContent='Login With Google'
          /> : <></>}
          <span className={classnames('divider-text-style', props.dividerTextStyle)}>{props.dividerText}</span>
          <FormInput
            htmlType='submit'
            formFinish={props.formFinish}
            formName={props.formName}
            formDetail={props.formDetail}
            buttonContent={props.buttonContent}
            buttonIcon={props.buttonIcon}
            buttonStyle={props.buttonStyle}

          />
          <div className='no-account-container'>
            <p>{props.noAccountText}</p>
            <Link to={props.linkNoAccount} className='link-no-account'>{props.noAccount}</Link>
          </div>
        </div>



      </div>
    </div>)
}
export default memo(PublicTemplete)
