export const LANG_PARAM = "lang";
export const LANGUAGE_CODE_DEFAULT = "en";
export const listLang = [
    {
        lang: "vi",
        name: "vietnamese"
    },
    {
        lang: "en",
        name: "english"
    }
];