// require('dotenv').config()
export const PUBLIC_ROUTE = {
    LOGIN: "/login",
    FORGOT_PASSWORD: '/forgot-password',
    PASSWORD_CODE_INPUT: '/password-code-confirm',
    RESET_PASSWORD: '/reset-password'
};

export const PRIVATE_ROUTE = {
    HOME: '/home',
    STUDENT_ATTENDANCE: '/student-attendance/:class-id',
    SEARCH_STUDENT: '/search-student',
    SCHEDULE: '/schedule',
    COURSE_LIST: '/course-list',
    COURSE_LEVEL_LIST: '/level:level-id',
    LESSON_LIST: '/lesson/:lesson-id',
    LESSON_DETAIL: '/:lesson-id',
    STUDENT_EVALUTION: '/'
    
};


export const ROUTE = {
    ...PUBLIC_ROUTE,
    ...PRIVATE_ROUTE
};

export const API_ROUTER = {
   
};

export const ROOT_API_URL = '';
export const ROOT_WS = 'localhost:3000';
